An fabricație: 2015

Producător: Opel

Model: Corsa

Caroserie: Hatchback

Transmisie: 5 viteze manuală

Carburant: Benzină

Stare: Puțin folosită

Locație: Chiajna

Putere: 70 CP

Tracțiune: Față

Motorizare: 1.2i

Culoare exterior: Gri

Culoare interior: Negru

Consum combustibil urban: 6.7 l/100 km

Consum combustibil extra-urban: 4.6 l/100 km

Consum combustibil mixt: 5.4 l/100 km

Emisii CO2: 126 g/km

Tipul de combustibil: Benzină

Accelerație 0-100 km/h: 16 sec

Viteza maximă: 162 km/h